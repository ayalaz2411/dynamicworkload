# auto-scaler.py

import os
import redis
import boto3
import subprocess

REDIS_HOST = os.getenv('REDIS_HOST')
# can be configured
JOBS_COUNT_NEEDS_SCALE_UP = 100
JOBS_COUNT_NEEDS_SCALE_DOWN = 10

running_instances = []

security_group = ""
key_path = "/home/ubuntu/key.pem"
keypair = boto3.resource('ec2').create_key_pair(KeyName='auto-scaler-key')
private_key_file = open(key_path, "w")
private_key_file.write(keypair.key_material)
private_key_file.close()

command = "chmod 400 {}".format(key_path)
subprocess.Popen(command.split(), stdout=subprocess.PIPE)

for x in boto3.resource('ec2').instances.limit(1):
    security_group = x.security_groups[0]['GroupName']

def scale_up():
    # Create new instance
    instances = boto3.resource('ec2').create_instances(
        ImageId="ami-042e8287309f5df03",
        MinCount=1,
        MaxCount=1,
        InstanceType="t3.micro",
        KeyName='auto-scaler-key',
        SecurityGroups=[security_group]
    )
    instances[0].wait_until_running()
    running_instances.append(instances[0])

    # Start worker on the new instance
    command = "bash /home/ubuntu/run_worker.sh {} {}".format(instances[0].private_ip_address, key_path)
    os.system(command)

def scale_down():
    # Terminate the last added instance
    running_instances.pop().terminate()

def entry():
    connection = redis.Redis(host=REDIS_HOST)

    while True:
        jobs_count = connection.llen('queue:jobs')

        if jobs_count > JOBS_COUNT_NEEDS_SCALE_UP:
            scale_up()

        if len(running_instances) > 1 and jobs_count < JOBS_COUNT_NEEDS_SCALE_DOWN:
            scale_down()

# Start with scaling up
scale_up()
entry()
