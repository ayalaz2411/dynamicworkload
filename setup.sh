# setup.sh

# debug
# set -o xtrace

# Set environment variables for AWS configuration
export AWS_DEFAULT_REGION=us-east-1
export AWS_ACCESS_KEY_ID=$(cat ./aws_access_key_id)
export AWS_SECRET_ACCESS_KEY=$(cat ./aws_secret_access_key)

# Check if AWS access keys are empty
if [ -z "$AWS_ACCESS_KEY_ID" ]; then
    echo "./aws_access_key_id is empty"
    exit 1
fi

if [ -z "$AWS_SECRET_ACCESS_KEY" ]; then
    echo "./aws_secret_access_key is empty"
    exit 1
fi

KEY_NAME="cloud-course-`date +'%N'`"
KEY_PEM="$KEY_NAME.pem"

# Create a key pair for connecting to instances and save it locally
echo "create key pair $KEY_PEM to connect to instances and save locally"
aws ec2 create-key-pair --key-name $KEY_NAME \
     | jq -r ".KeyMaterial" > $KEY_PEM

# Secure the key pair permissions
chmod 400 $KEY_PEM

SEC_GRP="my-sg-`date +'%N'`"

echo "setup firewall $SEC_GRP"
# Create a security group for instance access
aws ec2 create-security-group   \
    --group-name $SEC_GRP       \
    --description "Access my instances"

# Get the public IP of the current machine
MY_IP=$(curl ipinfo.io/ip)
echo "My IP: $MY_IP"

# Set up security group rules
echo "setup rule allowing SSH access to $MY_IP only"
aws ec2 authorize-security-group-ingress        \
    --group-name $SEC_GRP --port 22 --protocol tcp \
    --cidr $MY_IP/32

echo "setup rule allowing inside traffic"
aws ec2 authorize-security-group-ingress        \
    --group-name $SEC_GRP --source-group $SEC_GRP --protocol all

echo "setup rule allowing HTTP (port 5000) access to $MY_IP only"
aws ec2 authorize-security-group-ingress        \
    --group-name $SEC_GRP --port 5000 --protocol tcp \
    --cidr $MY_IP/32

UBUNTU_20_04_AMI="ami-042e8287309f5df03"
CREATE_INSTACNE_COMMAND="aws ec2 run-instances   \
    --image-id $UBUNTU_20_04_AMI        \
    --instance-type t3.micro            \
    --key-name $KEY_NAME                \
    --security-groups $SEC_GRP          \
    "

# Create instances and retrieve their instance IDs
SERVER_INSTANCE_ID=$(echo $($CREATE_INSTACNE_COMMAND) | jq -r '.Instances[0].InstanceId')
AUTOSCALER_INSTANCE_ID=$(echo $($CREATE_INSTACNE_COMMAND) | jq -r '.Instances[0].InstanceId')
REDIS_INSTANCE_ID=$(echo $($CREATE_INSTACNE_COMMAND) | jq -r '.Instances[0].InstanceId')

echo "Waiting for instance creation... $SERVER_INSTANCE_ID $REDIS_INSTANCE_ID $AUTOSCALER_INSTANCE_ID"
# Wait for the instances to be in the running state
aws ec2 wait instance-running --instance-ids $SERVER_INSTANCE_ID $REDIS_INSTANCE_ID $AUTOSCALER_INSTANCE_ID

# Get the public IP addresses of the instances
SERVER_PUBLIC_IP=$(aws ec2 describe-instances  --instance-ids $SERVER_INSTANCE_ID |
    jq -r '.Reservations[0].Instances[0].PublicIpAddress'
)

AUTOSCALER_PUBLIC_IP=$(aws ec2 describe-instances  --instance-ids $AUTOSCALER_INSTANCE_ID |
   
